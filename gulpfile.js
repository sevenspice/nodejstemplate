'use strict'

var del         = require('del');
var gulp        = require('gulp');
var babel       = require('gulp-babel');
var runSequence = require('run-sequence');
// JSの依存ライブラリ
var library = [];
// 定数
var SOURCE_DIR      = `${__dirname}/source`;
var DESTINATION_DIR = `${__dirname}/destination`;

// 初期化
gulp.task('clean', function(){
  'use strict'
  return del([`${DESTINATION_DIR}/**/*.js`], null);
});

/**
 * ES2015をコンパイルしてES5に変換するタスク
 */
gulp.task('es2015', function(){
  'use strict'

  return gulp.src((`${SOURCE_DIR}/**/*.js`))
    .pipe(babel({
      presets: ['es2015','stage-0']
    }))
    .pipe(gulp.dest((`${DESTINATION_DIR}`)));

});


/**
 * デフォルトタスク
 */
gulp.task('default', function(callback){
  'use strict';
  runSequence('clean', 'es2015', callback);
});
