/*
 * Javascriptにおける非同期関数の動作を確認するスクリプト
 * コールバックを使い非同期関数を同期的に使用する
 */

'use strict';
var utility = require(`${__dirname}/common/utility`);

/**
 * 同期的に一定時間処理を止めることを意図した関数
 * @param {integer}  wait     停止したいミリ秒を指定
 * @param {function} callback 停止後に実行する処理を指定
 */
var async_function = function (wait, callback) {
    utility.log('execute async_function.');

    // setTimeoutは非同期処理, 第2引数に指定した時間停止した後第1引数に指定されたコールバック関数の処理が走る
    setTimeout( () => {

        utility.log(`wait ${wait} ms.`);
        // ここでcallbackに指定された処理を呼ぶ
        callback();

    }, wait);

};

// ここから実行
utility.log('start script.');

// 3回実行
// コールバック関数内で繰り返し実行することで非同期関数を同期的に処理することができる
async_function(1000, () => {
    async_function(1000, () => {
        async_function(1000, () =>{

            utility.log('end script.');
            process.exit();

        });
    });
});

/*
-- 出力結果 --
time:[2017/6/21 15:58:0.825]    message:start script.
time:[2017/6/21 15:58:0.825]    message:execute async_function.
time:[2017/6/21 15:58:1.838]    message:wait 1000 ms.
time:[2017/6/21 15:58:1.838]    message:execute async_function.
time:[2017/6/21 15:58:2.853]    message:wait 1000 ms.
time:[2017/6/21 15:58:2.853]    message:execute async_function.
time:[2017/6/21 15:58:3.860]    message:wait 1000 ms.
time:[2017/6/21 15:58:3.860]    message:end script.
 */
