/**
 * Dateオブジェクトのフォーマットを担うクラス
 */
class DateFormatter {
    constructor (date) { }

    /**
     * Dateインスタンスを[yyyy/mm/dd hh:ii:ss.ms]形式の文字列に変換して返却する
     * @param {object} date フォーマットするDateオブジェクト
     */
    yyyymmddhhiissms (date) {

        // 年月日
        var yyyymmdd_l = [date.getFullYear(), (date.getMonth() + 1), date.getDate()];
        var yyyymmdd   = yyyymmdd_l.join('/');
        // 時分秒
        var hhiiss_l = [(date.getUTCHours() + 9), date.getMinutes(), date.getSeconds()];
        var hhiiss   = hhiiss_l.join(':');
        // ミリ秒
        var ms = date.getMilliseconds();

        // 結合して返却
        return `${yyyymmdd} ${hhiiss}.${ms}`;

    }

    /**
     * メソッドを実行した日付を[yyyy/mm/dd hh:ii:ss.ms]形式の文字列に変換して返却する
     */
    today_yyyymmddhhiissms () {
        return this.yyyymmddhhiissms(new Date());
    }

}

// クラスの出力
module.exports = DateFormatter;
