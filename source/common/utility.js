/**
 * 引数に指定された文字列に日付を付加して標準出力する
 * @param {string} message 表示したい文字列を指定
 */
var log = function (message) {

    var DateFormatter = require(`${__dirname}/dateformatter`);
    var dateFormetter = new DateFormatter();
    console.log(`time:[${dateFormetter.today_yyyymmddhhiissms()}]\tmessage:${message}`);

};

/**
 * メモリ使用量を標準出力する
 */
var memory_log = function () {

    var DateFormatter = require(`${__dirname}/dateformatter`);
    var dateFormetter = new DateFormatter();

    var usage = process.memoryUsage();
    var message = `time:[${dateFormetter.today_yyyymmddhhiissms()}]\trss:${usage.rss}\theapTotal:${usage. heapTotal}\theapUsed:${usage.heapUsed}`;
    console.log(message);

}

// 関数の出力
exports.log = log;
exports.memory_log = memory_log;
