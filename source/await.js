/*
 * 非同期関数を同期的に処理する場合はコールバックを使用する必要がある
 * しかしコールバックは処理内容が増えるほどネストが深くなる欠点がある
 * そこでPromise, async awaitを使用することで非同期関数をあたかも同期関数のように扱う
 */

'use strict';
// おまじない
require("babel-core/register");
require("babel-polyfill");

var utility = require(`${__dirname}/common/utility`);

/**
 * 同期的に一定時間処理を止めることを意図した関数
 * @param {integer} wait    停止したいミリ秒を指定
 * @param {string}  message 成功時に返却する文字列を指定
 */
var async_function = function (wait, message) {
    utility.log('execute async_function.');

    // Promiseオブジェクトを生成して返却する
    return new Promise( (resolve, reject) => {
        // Promiseオブジェクト内のコールバックに非同期処理を実行させる

        setTimeout( () => {

            utility.log(`wait ${wait} ms.`);
            // 問題なく実行された場合にresolveを呼び出す
            resolve(message);

        }, wait);

    });

};

// ここから実行
utility.log('start script.');

( async () => {
    
    // 3回実行
    // async, awaitの機能により非同期関数をあたかも同期関数の様に呼び出すことができる
    // 制約としてawaitはasync関数内でしか呼び出せない
    // awaitは成功するとresolveで渡された値を返却する
    var result1 = await async_function(1000, 'sucess');
    utility.log(result1);
    var result2 = await async_function(1000, 'sucess');
    utility.log(result2);
    var result3 = await async_function(1000, 'sucess');
    utility.log(result3);

})().then( () => {
    // async関数終了後に呼び出される

    utility.log('await process is all finished.');
    utility.log('end script.');

}).catch( (error) => {
    // async関数内で例外がスローされるとキャッチされる
    utility.log('Some error occurred async processing.');
});

/*
-- 出力結果 --
time:[2017/6/22 15:17:59.20]    message:start script.
time:[2017/6/22 15:17:59.22]    message:execute async_function.
time:[2017/6/22 15:18:0.28]     message:wait 1000 ms.
time:[2017/6/22 15:18:0.30]     message:sucess
time:[2017/6/22 15:18:0.32]     message:execute async_function.
time:[2017/6/22 15:18:1.33]     message:wait 1000 ms.
time:[2017/6/22 15:18:1.33]     message:sucess
time:[2017/6/22 15:18:1.34]     message:execute async_function.
time:[2017/6/22 15:18:2.34]     message:wait 1000 ms.
time:[2017/6/22 15:18:2.34]     message:sucess
time:[2017/6/22 15:18:2.35]     message:await process is all finished.
time:[2017/6/22 15:18:2.35]     message:end script.
 */
