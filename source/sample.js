/*
 * Javascriptにおける非同期関数の動作を確認するスクリプト
 */

'use strict';
var utility = require(`${__dirname}/common/utility`);

/**
 * 同期的に一定時間処理を止めることを意図した関数
 * ※ 実際は意図したとおりには動かない
 * @param {integer} wait 停止したいミリ秒を指定
 */
var async_function = function (wait) {
    utility.log('execute async_function.');

    // setTimeoutは非同期処理, 第2引数に指定した時間停止した後第1引数に指定されたコールバック関数の処理が走る
    setTimeout( () => {
        utility.log(`wait ${wait} ms.`);
    }, wait);

};

// ここから実行
utility.log('start script.');

// 3回実行
// 並列に動作して停止するため, 下記関数の実行はほぼ同時に終了する
async_function(1000);
async_function(1000);
async_function(1000);

utility.log('end script.');

/*
-- 出力結果 --
time:[2017/6/21 15:58:23.562]   message:start script.
time:[2017/6/21 15:58:23.563]   message:execute async_function.
time:[2017/6/21 15:58:23.563]   message:execute async_function.
time:[2017/6/21 15:58:23.563]   message:execute async_function.
time:[2017/6/21 15:58:23.563]   message:end script.
time:[2017/6/21 15:58:24.572]   message:wait 1000 ms.
time:[2017/6/21 15:58:24.572]   message:wait 1000 ms.
time:[2017/6/21 15:58:24.572]   message:wait 1000 ms.
 */
